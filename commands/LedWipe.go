package commands

import (
	"errors"
	"strconv"

	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/led-service/util"
	"gitlab.com/livesocket/service"
)

type LedWipe struct {
	Speed  int
	Colors []uint32
}

func NewLedWipe(args wamp.List) (*LedWipe, error) {
	if len(args) < 2 {
		return nil, errors.New("Missing args")
	}

	speed, err := strconv.Atoi(args[0].(string))
	if err != nil {
		return nil, err
	}

	colors := []uint32{}
	for _, item := range args[1:len(args)] {
		color, err := util.StringColorToUint32(item.(string))
		if err != nil {
			return nil, err
		}
		colors = append(colors, color)
	}

	return &LedWipe{Speed: speed, Colors: colors}, nil
}

func (l *LedWipe) Run() error {
	args := wamp.List{l.Speed}
	for _, color := range l.Colors {
		args = append(args, color)
	}

	_, err := service.Socket.SimpleCall("led.wipe", args, nil)
	if err != nil {
		return err
	}
	return nil
}
