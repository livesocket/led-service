package commands

type LedCommand interface {
	Run() error
}
