package commands

import (
	"errors"
	"strconv"

	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/led-service/util"
	"gitlab.com/livesocket/service"
)

type LedPulse struct {
	Speed int
	Size  int
	Color uint32
}

func NewLedPulse(args wamp.List) (*LedPulse, error) {
	if len(args) < 3 {
		return nil, errors.New("Missing args")
	}

	speed, err := strconv.Atoi(args[0].(string))
	if err != nil {
		return nil, err
	}
	size, err := strconv.Atoi(args[1].(string))
	if err != nil {
		return nil, err
	}
	color, err := util.StringColorToUint32(args[2].(string))
	if err != nil {
		return nil, err
	}

	return &LedPulse{Speed: speed, Size: size, Color: color}, nil
}

func (l *LedPulse) Run() error {
	_, err := service.Socket.SimpleCall("led.pulse", wamp.List{l.Speed, l.Size, l.Color}, nil)
	if err != nil {
		return err
	}
	return nil
}
