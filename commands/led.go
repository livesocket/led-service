package commands

import (
	"errors"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service/lib"
)

// LEDCommand
// !led set #FF0000
// !led wipe 2 #FF0000 #00FF00 #0000FF
// !led pulse 2 4 #FF00FF
var LEDCommand = lib.Action{
	Proc:    "command.led",
	Handler: led,
}

func led(invocation *wamp.Invocation) client.InvokeResult {
	command, err := getLedCommand(invocation.Arguments)
	if err != nil {
		return lib.ErrorResult(err)
	}

	command.Run()

	return client.InvokeResult{}
}

func getLedCommand(args wamp.List) (LedCommand, error) {

	if len(args) < 1 {
		return nil, errors.New("Missing subcommand")
	}

	if args[0].(string) == "set" {
		set, err := NewLedSet(args[1:len(args)])
		if err != nil {
			return nil, err
		}
		return set, nil
	}

	if args[0].(string) == "wipe" {
		set, err := NewLedWipe(args[1:len(args)])
		if err != nil {
			return nil, err
		}
		return set, nil
	}

	if args[0].(string) == "pulse" {
		set, err := NewLedPulse(args[1:len(args)])
		if err != nil {
			return nil, err
		}
		return set, nil
	}
	return nil, errors.New("Unrecognized sub command")
}
