package commands_test

import (
	"testing"

	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/led-service/commands"
)

func TestLedSetExtract(t *testing.T) {

	red := uint32(0xff0000)

	led := &commands.LedSet{}
	led.Extract(wamp.List{"#ff0000"})

	if led.Color != red {
		t.Error("Incorrect color (red)")
	}

	green := uint32(0x00ff00)

	led = &commands.LedSet{}
	led.Extract(wamp.List{"#00ff00"})

	if led.Color != green {
		t.Error("Incorrect color (green)")
	}

	blue := uint32(0x0000ff)

	led = &commands.LedSet{}
	led.Extract(wamp.List{"#0000ff"})

	if led.Color != blue {
		t.Error("Incorrect color (blue)")
	}

	white := uint32(0xffffff)

	led = &commands.LedSet{}
	led.Extract(wamp.List{"#ffffff"})

	if led.Color != white {
		t.Error("Incorrect color (white)")
	}

	random := uint32(0xfe527a)

	led = &commands.LedSet{}
	led.Extract(wamp.List{"#fe527a"})

	if led.Color != random {
		t.Error("Incorrect color (random)")
	}
}
