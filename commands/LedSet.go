package commands

import (
	"errors"

	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/led-service/util"
	"gitlab.com/livesocket/service"
)

type LedSet struct {
	Color uint32
}

func NewLedSet(args wamp.List) (*LedSet, error) {
	if len(args) < 1 {
		return nil, errors.New("Missing color")
	}

	color, err := util.StringColorToUint32(args[0].(string))
	if err != nil {
		return nil, err
	}

	return &LedSet{Color: color}, nil
}

func (l *LedSet) Run() error {
	_, err := service.Socket.SimpleCall("led.set", wamp.List{l.Color}, nil)
	if err != nil {
		return err
	}
	return nil
}
