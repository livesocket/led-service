module gitlab.com/livesocket/led-service

go 1.12

require (
	github.com/gammazero/nexus/v3 v3.0.0
	github.com/lucasb-eyer/go-colorful v1.0.2
	gitlab.com/livesocket/service v1.4.3
)
