package util

import "github.com/lucasb-eyer/go-colorful"

func StringColorToUint32(color string) (uint32, error) {
	c, err := colorful.Hex(color)
	if err != nil {
		return 0, err
	}
	r, g, b := c.RGB255()
	return (uint32(r) << 16) + (uint32(g) << 8) + uint32(b), nil
}
