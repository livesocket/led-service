package main

import (
	"log"
	"os"
	"os/signal"

	"gitlab.com/livesocket/led-service/commands"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func main() {
	close, err := service.NewStandardService([]lib.Action{
		commands.LEDCommand,
	}, nil, "")
	defer close()
	if err != nil {
		panic(err)
	}

	// Wait for CTRL-c or client close while handling events.
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	select {
	case <-sigChan:
	case <-service.Socket.Done():
		log.Print("Router gone, exiting")
		return
	}
}
