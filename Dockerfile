FROM golang:1.12-alpine as base
RUN apk --no-cache add git ca-certificates g++
WORKDIR /repos/led-service
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/led-service
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o led-service

FROM scratch as release
COPY --from=builder /repos/led-service/led-service /led-service
EXPOSE 8080
ENTRYPOINT ["/led-service"]
